import './App.css';
import Navbar from './components/Navbar';
import Banner from './components/Banner';
import Cards from './components/cards';
import Search from './components/search';
import Logo from './components/logo';
import Footer from './components/footer'

function App() {
  return (
    <div>
        <Navbar/>
        <Banner />
        <Cards/>
        <Search/>
        <Logo/>
        <Footer/>
    </div>
  );
}

export default App;
