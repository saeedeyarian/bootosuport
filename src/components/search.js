import React, { Component } from 'react'
import styles from './search.module.scss';

class Search extends Component {

    
    componentDidMount () {
        this.input.current.focus();
    }

    constructor(props){
        super(props);
        this.input = React.createRef();
        this.state = {
            text: ""
        }
    }

    changeHandler = event => {
        this.setState ({
            text: event.target.value,
        })
    }

    render() {
        return (
            <div className={styles.container}>
                <p>Search What You Want</p>

                <div>
                    <input placeholder='search...' value={this.state.text} onChange={this.changeHandler} ref={this.input}/>
                    <br />
                    <br />
                    <span>{this.state.text}</span>
                </div>
            </div>
        )
    }
}


export default Search;