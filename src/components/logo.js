import React, { Component } from 'react'
import styles from './logo.module.scss'
import apple from '../images/apple.jpg';
import xiaomi from '../images/xiaomi.png';
import samsung from '../images/samsung.jpg'
class Logo extends Component {
    render() {
        return (
            <div className={styles.container}>
                <p>Who Supoport Us?</p>

                <div>
                    <img src={apple} alt='brand'/>
                    <img src={xiaomi} alt='brand'/>
                    <img src={samsung} alt='brand'/>
                </div>
            </div>
        )
    }
}



export default Logo;