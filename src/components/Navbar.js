import React, { Component } from 'react'
import styles from './Navbar.module.scss'
import logo from '../images/Logo.png'

class Navbar extends Component {
    render() {
        return (
            <header className={styles.header}>
                <div className={styles.navbarList}>
                <ul className={styles.navbar}>
                        <li>Home Page</li>
                        <li>Products</li>
                        <li>About Us</li>
                    </ul>
                </div>
                <div className={styles.logo}>
                    <img className={styles.logo} src={logo} alt="Logo"/>
                </div>
            </header>
        )
    }
}

export default Navbar;
