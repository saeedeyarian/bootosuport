import React, { Component } from 'react'
import styles from './Banner.module.scss'
import banner from '../images/banner.jpg'

class Banner extends Component {
    render() {
        return (
            <div className={styles.container}>
                <img className={styles.Banner} alt='Banner' src={banner}/>
                <div className={styles.textContainer}>
                    <h1>Botostart</h1>
                    <p>
                        We're learning <span>React.js</span>
                    </p>
                </div>
            </div>
        )
    }
}

export default Banner;