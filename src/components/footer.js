import React, { Component } from 'react'
import styles from './footer.module.scss'

class Footer extends Component {
    render() {
        return (
            <div className={styles.container}>
                <p>© All Rights Reserved 2021</p>
            </div>
        )
    }
}

export default Footer;
