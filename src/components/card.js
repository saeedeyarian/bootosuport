import React, { Component } from 'react'
import styles from './card.module.scss';
import down from '../images/down.svg';
import up from '../images/up.svg'

class Card extends Component {

    constructor () {
        super();
        this.state = {
            counter : 0,
        }
    }

    downHandler = () => {
        if (this.state.counter >= 1) {
            this.setState (prevState => ({
                counter: prevState.counter -1
            }))
        }
    }

    upHandler = () => {
        this.setState(prevState => ({
            counter : prevState.counter +1
        }))
    }

    render() {
        const {counter} = this.state
        const {image,name,cost} = this.props
        return (
            <div className={styles.container}>
                <img className={styles.image} src={image} alt='smart phone'/>
                <h3>{name}</h3>
                <p>{cost} {counter ? ` price =${counter *  Number(cost.split(" ")[0])} $` : ""}</p>
                
                <div className={styles.counterBox}>
                    <img className={!counter && styles.deactive} src={down} alt="arrow" onClick={this.downHandler} />
                    <span>{counter}</span>
                    <img src={up} alt='arrow' onClick={this.upHandler}/>
                </div>
            </div>
        )
    }
}


export default Card;