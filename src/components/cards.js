import React, { Component } from 'react';
import Card from './card';
import styles from './cards.module.scss';

import iphone10 from '../images/iphone10.jpg';
import iphone11 from '../images/iphone11.jpg';
import iphone12 from '../images/iphone12.jpg';
import s21 from '../images/s21.jpg';

class Cards extends Component {

    constructor () {
        super();
        this.state = {
            phoneData :[
                {id: 1,image:iphone10, name:"iphoneX", cost:"500 $"},
                {id: 2,image:iphone11 ,name:"iphone13 pro", cost:"1200 $"},
                {id: 3,image:iphone12,name:"iphone11", cost:"700 $"},
                {id: 4,image:s21 ,name:"iphone13", cost:"1000 $"}
            ]
        }
      }
    render() {

        return (
            <div className={styles.container}>
                {this.state.phoneData.map( phone => 
                    <Card image={phone.image} name={phone.name} key={phone.id} cost={phone.cost}/>)}
            </div>
        )
    }
}
export default Cards;